﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Db4oDatabase.Models;

namespace Db4oDatabase.Utils
{
    public static class Tools
    {
        public static List<FileInfo> ShowExistingDbs()
        {
            var startupPath = System.IO.Directory.GetCurrentDirectory();
            var d = new DirectoryInfo(startupPath);
            var files = d.GetFiles("*.db4o").ToList();
            return files;
        }

        public static void ShowPerson(Person person)
        {
            Console.WriteLine($"Imie: {person.FirstName}, Nazwisko: {person.SecondName}");
            Console.WriteLine(person.Address != null
                ? $"Ulica: {person.Address.Street}, Kod pocztowy: {person.Address.PostalCode}, Miasto: {person.Address.City}"
                : "Brak wprowadzonego adresu");
            if (person.PhonesList.Any())
                foreach (var phone in person.PhonesList)
                    Console.WriteLine($"Numer: {phone.Number}, Operator: {phone.Operator}, Typ: {phone.PhoneType}");
            else
                Console.WriteLine("Brak wprowadzonych numerow");
            Console.WriteLine("_________________________________________________________");
        }
    }
}