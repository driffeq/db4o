﻿using System.Collections.Generic;

namespace Db4oDatabase.Models
{
    public class Person
    {
        public Person(string firstName, string secondName, Address address = null, List<Phone> phonesList = null)
        {
            FirstName = firstName;
            SecondName = secondName;
            Address = address;
            PhonesList = phonesList ?? new List<Phone>();
        }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public Address Address { get; set; }
        public List<Phone> PhonesList { get; set; }
    }
}