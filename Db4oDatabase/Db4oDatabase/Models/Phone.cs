﻿using Db4oDatabase.Enums;

namespace Db4oDatabase.Models
{
    public class Phone
    {
        public string Number { get; set; }
        public Operator Operator { get; set; }
        public PhoneType PhoneType { get; set; }
    }
}