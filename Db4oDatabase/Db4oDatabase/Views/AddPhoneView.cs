﻿using System;
using System.Linq;
using Db4oDatabase.Enums;
using Db4oDatabase.Models;

namespace Db4oDatabase.Views
{
    public static class AddPhoneView
    {
        public static void Show(Person person)
        {
            var phone = new Phone();
            Console.Write("Podaj numer telefonu: ");
            phone.Number = Console.ReadLine();
            Console.WriteLine("Wybierz operatora: ");
            for (var i = 1; i < Enum.GetNames(typeof(Operator)).Length + 1; i++)
                Console.WriteLine($"\t{i}. {Enum.GetNames(typeof(Operator)).ElementAt(i - 1)}");
            var selectedOperator = Convert.ToInt32(Console.ReadLine());

            phone.Operator = (Operator) selectedOperator;
            Console.WriteLine("Wybierz typ: ");
            for (var i = 1; i < Enum.GetNames(typeof(PhoneType)).Length + 1; i++)
                Console.WriteLine($"\t{i}. {Enum.GetNames(typeof(PhoneType)).ElementAt(i - 1)}");
            var selectedPhoneType = Convert.ToInt32(Console.ReadLine());
            phone.PhoneType = (PhoneType) selectedPhoneType;
            person.PhonesList.Add(phone);
        }
    }
}