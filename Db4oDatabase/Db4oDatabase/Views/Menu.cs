﻿using System;
using System.Linq;
using Db4oDatabase.DAL;
using Db4oDatabase.Models;
using Db4oDatabase.Utils;

namespace Db4oDatabase.Views
{
    public class Menu
    {
        public Menu()
        {
            ChooseDb();
        }
        public void ChooseDb()
        {
            Console.WriteLine("Wpisz nazwe istniejacej bazy lub stworz nowa");
            Console.WriteLine("Lista dostepnych baz");
            foreach (var item in Tools.ShowExistingDbs())
                Console.WriteLine(item.Name.Remove(item.Name.Length - 5, 5));
            var fileName = Console.ReadLine();
            if (fileName != string.Empty)
                Database.AccessDatabase(fileName);
            else
                return;

            ShowMainMenu();
        }

        public void ShowMainMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Pokaz wszystkie osoby");
                Console.WriteLine("2. Dodaj osobe");
                Console.WriteLine("3. Usun osobe");
                Console.WriteLine("4. Edytuj osobe");
                Console.WriteLine("5. Statystyka");
                Console.WriteLine("6. Wyjscie");
                var x = Console.ReadLine();
                var choice = x != string.Empty ? Convert.ToInt32(x) : 6;


                switch (choice)
                {
                    case 1:
                        ShowAllPeople();
                        break;
                    case 2:
                        ShowAddPerson();
                        break;
                    case 3:
                        ShowDeletePerson();
                        break;
                    case 4:
                        ShowUpdatePerson();
                        break;
                    case 5:
                        ShowStatistics();
                        break;
                    case 6:
                        return;
                    default:
                        continue;
                }
                break;
            }
        }

        public void ShowAllPeople()
        {
            Console.Clear();
            var people = Database.GetAllPeopleList();
            foreach (var item in people)
                Tools.ShowPerson(item);
            Console.ReadLine();
            ShowMainMenu();
        }

        public void ShowAddPerson()
        {
            Console.Clear();
            Console.Write("Podaj imie: ");
            var firstName = Console.ReadLine();
            Console.Write("Podaj nazwisko: ");
            var secondName = Console.ReadLine();
            var person = new Person(firstName, secondName);
            Console.Write("Czy chcesz dodac adres? t/n\t");
            var choiceAdr = Console.ReadKey();
            Console.WriteLine();
            if (choiceAdr.KeyChar == 't')
            {
                person.Address = new Address();
                Console.Write("Podaj ulice: ");
                person.Address.Street = Console.ReadLine();
                Console.Write("Podaj kod pocztowy: ");
                person.Address.PostalCode = Console.ReadLine();
                Console.Write("Podaj miasto: ");
                person.Address.City = Console.ReadLine();
            }
            Console.Write("Czy chcesz dodac telefon? t/n\t");
            var choiceTel = Console.ReadKey();
            Console.WriteLine();
            if (choiceTel.KeyChar == 't')
                AddPhoneView.Show(person);
            Database.AddPerson(person);
            Console.WriteLine($"Dodano {person.FirstName} {person.SecondName}");
            Console.ReadLine();
            ShowMainMenu();
        }

        public void ShowDeletePerson()
        {
            Console.Clear();
            Console.Write("Podaj imie osoby do usuniecia: ");
            var firstname = Console.ReadLine();
            Console.Write("Podaj nazwisko osoby do usuniecia: ");
            var secondname = Console.ReadLine();
            var person = Database.GetPerson(firstname, secondname);
            if (person != null)
            {
                Database.DeletePerson(person);
                Console.WriteLine($"Usunieto {firstname} {secondname}");
            }
            else
            {
                Console.WriteLine("Nie ma takiej osoby w bazie");
            }
            Console.ReadLine();
            ShowMainMenu();
        }

        public void ShowUpdatePerson()
        {
            Console.Clear();
            Console.Write("Podaj imie osoby do edycji: ");
            var firstname = Console.ReadLine();
            Console.Write("Podaj nazwisko osoby do edycji: ");
            var secondname = Console.ReadLine();
            var person = Database.GetPerson(firstname, secondname);
            if (person != null)
            {
                ShowUpdatePersonDetails(person);
            }
            else
            {
                Console.Write("Nie ma takiej osoby w bazie, czy chcesz ja stworzyc? t/n\t");
                var key = Console.ReadKey();
                if (key.KeyChar != 't') ShowMainMenu();
                var personToAdd = new Person(firstname, secondname);
                Database.AddPerson(personToAdd);
                ShowUpdatePersonDetails(personToAdd);
            }
        }

        public void ShowUpdatePersonDetails(Person person)
        {
            while (true)
            {
                Console.Clear();
                Tools.ShowPerson(person);
                Console.WriteLine("1. Modyfikuj dane osoby");
                Console.WriteLine("2. Dodaj adres osoby ");
                Console.WriteLine("3. Modyfikuj adres osoby");
                Console.WriteLine("4. Usun adres osoby");
                Console.WriteLine("5. Dodaj telefon");
                Console.WriteLine("6. Usun telefon");
                Console.WriteLine("7. Powrot do glownego menu");
                var x = Console.ReadLine();
                var choice = x != string.Empty ? Convert.ToInt32(x) : 7;
                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        Console.Write("Podaj nowe imie: ");
                        person.FirstName = Console.ReadLine();
                        Console.Write("Podaj nowe nazwisko: ");
                        person.SecondName = Console.ReadLine();
                        Database.UpdatePerson(person);
                        Console.WriteLine("Zaktualizowano");
                        Console.ReadLine();
                        ShowUpdatePersonDetails(person);
                        break;
                    case 2:
                        Console.Clear();
                        if (person.Address == null)
                        {
                            var addressAdd = new Address();
                            Console.Write("Podaj ulice: ");
                            addressAdd.Street = Console.ReadLine();
                            Console.Write("Podaj kod pocztowy: ");
                            addressAdd.PostalCode = Console.ReadLine();
                            Console.Write("Podaj miasto: ");
                            addressAdd.City = Console.ReadLine();
                            person.Address = addressAdd;
                            Database.UpdatePerson(person);
                            Console.WriteLine("Dodano");
                            Console.ReadLine();
                            ShowUpdatePersonDetails(person);
                        }
                        else
                        {
                            Console.WriteLine("Osoba posiada wpisany adres, wybierz opcje modyfikacji.");
                            Console.ReadLine();
                            ShowUpdatePersonDetails(person);
                        }
                        break;
                    case 3:
                        Console.Clear();
                        var addressModify = Database.GetAddress(person);
                        Console.Write("Podaj ulice: ");
                        addressModify.Street = Console.ReadLine();
                        Console.Write("Podaj kod pocztowy: ");
                        addressModify.PostalCode = Console.ReadLine();
                        Console.Write("Podaj miasto: ");
                        addressModify.City = Console.ReadLine();
                        person.Address = addressModify;
                        Database.UpdatePerson(person);
                        Console.WriteLine("Zaktualizowano");
                        Console.ReadLine();
                        ShowUpdatePersonDetails(person);
                        break;
                    case 4:
                        Console.Clear();
                        if (person.Address != null)
                        {
                            var addressToDelete = Database.GetAddress(person);
                            Database.DeleteAddress(addressToDelete);
                            person.Address = null;
                            Console.WriteLine("Usunieto");
                            Console.ReadLine();
                            ShowUpdatePersonDetails(person);
                        }
                        else
                        {
                            Console.WriteLine("Osoba nie posiada adresu do usuniecia.");
                            Console.ReadLine();
                            ShowUpdatePersonDetails(person);
                        }
                        break;
                    case 5:
                        Console.Clear();
                        AddPhoneView.Show(person);
                        Database.UpdatePerson(person);
                        Console.WriteLine("Dodano");
                        Console.ReadLine();
                        ShowUpdatePersonDetails(person);
                        break;
                    case 6:
                        Console.Clear();
                        if (person.PhonesList.Any())
                        {
                            Console.WriteLine("Wybierz numer do usuniecia");
                            for (var i = 1; i < person.PhonesList.Count + 1; i++)
                                Console.WriteLine($"{i}. {person.PhonesList.ElementAt(i - 1).Number}");
                            var selected = Convert.ToInt32(Console.ReadLine());
                            var phoneToDelete = Database.GetPhone(person, selected);
                            Database.DeletePhone(phoneToDelete);
                            person.PhonesList.RemoveAt(selected - 1);
                            Database.UpdatePerson(person);
                            Console.WriteLine("Usunieto");
                            Console.ReadLine();
                            ShowUpdatePersonDetails(person);
                        }
                        break;
                    case 7:
                        ShowMainMenu();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Bledna operacja");
                        Console.ReadLine();
                        continue;
                }
                break;
            }
        }

        public void ShowStatistics()
        {
            Console.Clear();
            var personAmount = Database.GetAllPeopleList().Count;
            var phoneAmount = Database.GetAllPhones().Count;
            var addressAmount = Database.GetAllAddresses().Count;
            Console.WriteLine($"Ilosc osob w bazie: {personAmount}");
            Console.WriteLine($"Ilosc telefonow w bazie: {phoneAmount}");
            Console.WriteLine($"Ilosc adresow w bazie: {addressAmount}");
            Console.ReadLine();
            ShowMainMenu();
        }
    }
}