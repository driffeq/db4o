﻿namespace Db4oDatabase.Enums
{
    public enum Operator : byte
    {
        P4 = 1,
        Orange = 2,
        Polkomtel = 3
    }

    public enum PhoneType : byte
    {
        Mobile = 1,
        Home = 2
    }
}