﻿using Db4oDatabase.DAL;
using Db4oDatabase.Views;

namespace Db4oDatabase
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var menu = new Menu();
            if (Database._db == null) return;
            Database.CloseDatabase();
        }
    }
}