﻿using System.Collections.Generic;
using System.Linq;
using Db4objects.Db4o;
using Db4objects.Db4o.Config;
using Db4oDatabase.Models;

namespace Db4oDatabase.DAL
{
    public static class Database
    {
        private static IEmbeddedConfiguration _config;
        public static IObjectContainer _db { get; private set; }

        public static void AccessDatabase(string fileName)
        {
            fileName = fileName + ".db4o";
            _config = Db4oEmbedded.NewConfiguration();
            _config.Common.ObjectClass(typeof(Person)).CascadeOnUpdate(true);
            _config.Common.ObjectClass(typeof(Person)).CascadeOnDelete(true);
            _db = Db4oEmbedded.OpenFile(_config, fileName);
        }

        public static void CloseDatabase()
        {
            _db.Close();
        }

        public static IList<Person> GetAllPeopleList()
        {
            return _db.Query<Person>();
        }

        public static Person GetPerson(string firstname, string secondname)
        {
            return _db.Query<Person>(x => x.FirstName == firstname && x.SecondName == secondname)
                .FirstOrDefault();
        }

        public static void AddPerson(Person person)
        {
            _db.Store(person);
            _db.Commit();
        }

        public static void DeletePerson(Person person)
        {
            _db.Delete(person);
            _db.Commit();
        }

        public static void UpdatePerson(Person person)
        {
            _db.Store(person);
            _db.Commit();
        }

        public static Address GetAddress(Person person)
        {
            return _db.Query<Address>(x => x.Street == person.Address.Street).FirstOrDefault();
        }

        public static void DeleteAddress(Address address)
        {
            _db.Delete(address);
            _db.Commit();
        }

        public static Phone GetPhone(Person person, int selected)
        {
            return _db.Query<Phone>(x => x.Number == person.PhonesList.ElementAt(selected - 1).Number).FirstOrDefault();
        }

        public static void DeletePhone(Phone phone)
        {
            _db.Delete(phone);
            _db.Commit();
        }

        public static IList<Address> GetAllAddresses()
        {
            return _db.Query<Address>();
        }

        public static IList<Phone> GetAllPhones()
        {
            return _db.Query<Phone>();
        }
    }
}